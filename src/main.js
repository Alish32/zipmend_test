import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vClickOutside from 'v-click-outside'

import BaseButton from '@/UI/TheBaseButton'
import BaseCard from '@/UI/TheBaseCard'
import BaseInput from '@/UI/TheBaseInput'
import SpinnerCircle from '@/components/SpinnerCircle'

Vue.use(vClickOutside)

Vue.component('base-button', BaseButton)
Vue.component('base-card', BaseCard)
Vue.component('base-input', BaseInput)
Vue.component('spinner-circle', SpinnerCircle)

Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
