import Vue from 'vue'
import VueRouter from 'vue-router'
import PriceCalculator from '../views/PriceCalculator.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'PriceCalculator',
    component: PriceCalculator
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
