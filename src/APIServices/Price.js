import axios from 'axios';
import { BASE_URL } from './Variables'
const API_URL_PRICE = `${BASE_URL}/price`

export const PRICE = {
    price(data) {
        const url = `${API_URL_PRICE}`;
        return axios.post(url, data);
    },
}