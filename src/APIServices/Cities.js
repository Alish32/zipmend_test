import axios from 'axios';
import { BASE_URL } from './Variables'
const API_URL_CITIES = `${BASE_URL}/cities`

export const CITIES = {
    cities(country, query) {
        const url = `${API_URL_CITIES}/${country}/${query}`;
        return axios.get(url);
    },
}