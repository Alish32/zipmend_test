import axios from 'axios';

import { CITIES } from './Cities'
import { PRICE } from './Price'

export default {
    ...CITIES,
    ...PRICE,
}
axios.interceptors.request.use(
    config => {
        config.headers = Object.assign({
            // Authorization: 'tb7iVecOI1XQcj58346jN2bQJ8MrDG7YXwr4NXvS',
        }, config.headers);
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use((response) => response, (error) => {
    if (error.response.status == 401) {
        alert('unauthorized user')
    }
    throw error;
});